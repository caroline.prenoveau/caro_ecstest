﻿using UnityEngine;
using HotChocolate.UI;
using System;
using System.Collections;
using Unity.Entities;
using Unity.Collections;

public class Game : MonoBehaviour
{
    public GameSettings settings;

    public MenuDefinition gameHud;
    public MenuDefinition gameOverView;
    public MenuStack menuStack;

    public GameData Data { get; private set; }

    private World world;

    private void Start()
    {
        Data = new GameData(settings.playerSettings.playerCount);

        world = World.DefaultGameObjectInjectionWorld;
        StartCoroutine(PlayForever());
    }

    public IEnumerator PlayForever()
    {
        while(true)
        {
            yield return Play();
        }
    }

    public IEnumerator Play()
    {
        StartGame();

        menuStack.Jump(gameHud.prefabRef, gameHud.id, null, new UI.GameHudData() { gameData = Data });

        var playerSystem = world.GetOrCreateSystem<PlayerSystem>();

        foreach(var player in Data.players)
        {
            player.CurrentScore = 0;
        }

        while (!playerSystem.IsGameOver())
        {
            for(int i = 0; i < Data.players.Count; ++i)
            {
                var playerState = playerSystem.PlayerState(i);

                if(playerState.score.HasValue) Data.players[i].CurrentScore = playerState.score.Value;
                Data.players[i].Lives = playerState.lives;

                if (!Data.players[i].BestScore.HasValue || Data.players[i].CurrentScore > Data.players[i].BestScore.Value)
                {
                    Data.players[i].BestScore = Data.players[i].CurrentScore;
                }
            }

            yield return null;
        }

        foreach (var player in Data.players)
        {
            player.Lives = 0;
        }

        StopGame();

        menuStack.Push(gameOverView.prefabRef, gameOverView.id);

        while (!Input.GetKeyDown(KeyCode.Return))
        {
            yield return null;
        }

        menuStack.Pop();
    }

    private void StartGame()
    {
        var playerSystem = world.GetOrCreateSystem<PlayerSystem>();
        playerSystem.StartSystem(settings.playerSettings, PrefabEntityConverter.playerShipEntity);
        playerSystem.SpawnAll();

        var spawnSystem = world.GetOrCreateSystem<AsteroidSpawnSystem>();
        spawnSystem.StartSystem(
            settings.asteroidSpawnSettings,
            PrefabEntityConverter.bigAsteroidEntity,
            PrefabEntityConverter.mediumAsteroidEntity,
            PrefabEntityConverter.smallAsteroidEntity);

        spawnSystem.DestroyAll();
    }

    private void StopGame()
    {
        var spawnSystem = world.GetOrCreateSystem<AsteroidSpawnSystem>();
        spawnSystem.StopSystem();

        var playerSystem = world.GetOrCreateSystem<PlayerSystem>();
        playerSystem.StopSystem();
    }
}