﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Game Settings", order = 1)]
public class GameSettings : ScriptableObject
{
    public AsteroidSpawnSettings asteroidSpawnSettings;
    public PlayerSettings playerSettings;
}