﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

[System.Serializable]
public struct PlayerSettings
{
    public int playerCount;
    public int playerHealth;
    public int playerLives;
    public List<PlayerInputs> playerInputs;
}

public class PlayerSystem : SystemBase
{
    private PlayerSettings settings;
    private Entity playerShipPrefab;

    private bool started;

    private BeginInitializationEntityCommandBufferSystem beginInitEntityCommandBufferSystem;

    public void StartSystem(PlayerSettings settings, Entity playerShipPrefab)
    {
        this.settings = settings;
        this.playerShipPrefab = playerShipPrefab;

        started = true;
    }

    public void StopSystem()
    {
        started = false;
    }

    public bool IsGameOver()
    {
        EntityQuery playersQuery = GetEntityQuery(ComponentType.ReadOnly<Player>());
        return playersQuery.CalculateEntityCount() == 0;
    }

    public (int? score, int lives) PlayerState(int playerIndex)
    {
        EntityQuery playersQuery = GetEntityQuery(ComponentType.ReadOnly<Player>());
        if (playersQuery.CalculateEntityCount() == 0)
            return (null, 0);

        using (NativeArray<Player> players = playersQuery.ToComponentDataArray<Player>(Allocator.Temp))
        {
            foreach(var player in players)
            {
                if(player.playerIndex == playerIndex)
                    return (player.score, player.lives);
            }
        }

        return (null, 0);
    }

    public void SpawnAll()
    {
        for (int i = 0; i < settings.playerCount && i < settings.playerInputs.Count; ++i)
        {
            SpawnPlayer(i, settings.playerInputs[i], settings.playerHealth, settings.playerLives, playerShipPrefab);
        }
    }

    private void SpawnPlayer(int playerIndex, PlayerInputs controls, int playerHealth, int playerLives, Entity playerShipPrefab)
    {
        var instance = EntityManager.Instantiate(playerShipPrefab);

        EntityManager.AddComponent(instance, typeof(Player));
        EntityManager.AddComponent(instance, typeof(Health));
        EntityManager.AddBuffer<DamageEvent>(instance);

        EntityManager.SetComponentData(instance, new Player
        {
            playerIndex = playerIndex,
            inputs = controls,
            lives = playerLives,
            ship = instance
        });
        EntityManager.SetComponentData(instance, new Health
        {
            amount = playerHealth,
            maxAmount = playerHealth,
            autoDestruct = false
        });
        EntityManager.SetComponentData(instance, new Translation
        {
            Value = Vector3.zero
        });
    }

    protected override void OnCreate()
    {
        beginInitEntityCommandBufferSystem = World.GetExistingSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        if (!started)
            return;

        ControlShip();
        CheckHealth();
    }

    private void ControlShip()
    {
        Entities.ForEach((
            Entity entity,
            int entityInQueryIndex,
            in Health health,
            in Player player) =>
        {
            if (health.amount <= 0)
            {
                return;
            }

            if (HasComponent<Ship>(player.ship))
            {
                Ship ship = GetComponent<Ship>(player.ship);

                ship.rotateLeft = Input.GetKey(player.inputs.rotateLeft);
                ship.rotateRight = Input.GetKey(player.inputs.rotateRight);
                ship.thrust = Input.GetKey(player.inputs.thrust);

                if (Input.GetKeyDown(player.inputs.shoot))
                {
                    ship.shootTrigger = true;
                }

                SetComponent(entity, ship);
            }
        }).Run();
    }

    private void CheckHealth()
    {
        EntityCommandBuffer ecbPackage = beginInitEntityCommandBufferSystem.CreateCommandBuffer();
        EntityCommandBuffer.ParallelWriter ecb = ecbPackage.AsParallelWriter();

        Entities.ForEach((
            Entity entity,
            int entityInQueryIndex,
            ref Translation trans,
            ref Rotation rot,
            ref PhysicsVelocity vel,
            ref Health health,
            ref Player player) =>
        {
            if (health.amount <= 0)
            {
                health.amount = health.maxAmount;
                player.lives--;

                if (player.lives <= 0)
                {
                    ecb.DestroyEntity(entityInQueryIndex, entity);
                }
                else
                {
                    trans.Value = float3.zero;
                    rot.Value = quaternion.identity;
                    vel.Linear = float3.zero;
                    vel.Angular = float3.zero;
                }
            }
        }).ScheduleParallel();

        beginInitEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
