﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Physics.Systems;
using Unity.Transforms;

[UpdateBefore(typeof(BuildPhysicsWorld))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class ShipSystem : SystemBase
{
    private BeginInitializationEntityCommandBufferSystem beginInitEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        beginInitEntityCommandBufferSystem = World.GetExistingSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        EntityCommandBuffer ecb = beginInitEntityCommandBufferSystem.CreateCommandBuffer();

        Entities.ForEach((
            Entity entity,
            int entityInQueryIndex,
            ref Rotation rot,
            ref PhysicsVelocity vel,
            ref Ship ship,
            in PhysicsMass mass,
            in LocalToWorld ltw) =>
        {
            if (ship.rotateLeft)
            {
                rot.Value = math.mul(rot.Value, quaternion.RotateY(-ship.rotationSpeed * deltaTime));
            }
            if (ship.rotateRight)
            {
                rot.Value = math.mul(rot.Value, quaternion.RotateY(ship.rotationSpeed * deltaTime));
            }
            if (ship.thrust)
            {
                vel.ApplyLinearImpulse(mass, ltw.Forward * ship.thrustForce * deltaTime);
            }
            if (ship.shootTrigger)
            {
                LocalToWorld spawnPointLTW = GetComponent<LocalToWorld>(ship.bulletSpawnPoint);

                Entity spawnedBulletEntity = ecb.Instantiate(ship.bulletPrefab);
                ecb.SetComponent(spawnedBulletEntity, new Translation { Value = spawnPointLTW.Position });
                ecb.AddComponent(spawnedBulletEntity, new DamageSource { damageAmount = 1, sourceEntity = entity });
                ecb.AddComponent(spawnedBulletEntity, new Health { amount = 1, maxAmount = 1, autoDestruct = true });
                ecb.AddBuffer<DamageEvent>(spawnedBulletEntity);
                ecb.SetComponent(spawnedBulletEntity, new PhysicsVelocity { Linear = ltw.Forward * ship.bulletSpeed });

                ship.shootTrigger = false;
            }
        }).Schedule();

        beginInitEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
