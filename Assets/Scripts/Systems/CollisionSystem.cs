﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class CollisionSystem : SystemBase
{
    private BuildPhysicsWorld buildPhysicsWorldSystem;
    private StepPhysicsWorld stepPhysicsWorldSystem;
    private EntityQuery damageSourceQuery;

    protected override void OnCreate()
    {
        buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();

        damageSourceQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[]
            {
                typeof(DamageSource)
            }
        });
    }

    [BurstCompile]
    struct CollideJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<DamageSource> DamageSource;
        [ReadOnly] public ComponentDataFromEntity<Health> Health;
        public BufferFromEntity<DamageEvent> DamageEvent;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity entityA = triggerEvent.EntityA;
            Entity entityB = triggerEvent.EntityB;

            bool isBodyADamageSource = DamageSource.HasComponent(entityA);
            bool isBodyBDamageSource = DamageSource.HasComponent(entityB);

            bool isBodyADamageable = Health.HasComponent(entityA);
            bool isBodyBDamageable = Health.HasComponent(entityB);

            if(isBodyADamageSource && isBodyBDamageable)
            {
                var damageSourceA = DamageSource[entityA];
                var healthB = Health[entityB];

                if (healthB.amount > 0 && DamageEvent.HasComponent(entityB))
                {
                    DamageEvent[entityB].Add(new DamageEvent()
                    {
                        damageAmount = damageSourceA.damageAmount,
                        damageSource = damageSourceA.sourceEntity
                    });
                }
            }

            if(isBodyBDamageSource && isBodyADamageable)
            {
                var damageSourceB = DamageSource[entityB];
                var healthA = Health[entityA];

                if (healthA.amount > 0 && DamageEvent.HasComponent(entityA))
                {
                    DamageEvent[entityA].Add(new DamageEvent()
                    {
                        damageAmount = damageSourceB.damageAmount,
                        damageSource = damageSourceB.sourceEntity
                    });
                }
            }
        }
    }

    protected override void OnUpdate()
    {
        if (damageSourceQuery.CalculateEntityCount() == 0)
        {
            return;
        }

        Dependency = new CollideJob
        {
            DamageSource = GetComponentDataFromEntity<DamageSource>(true),
            Health = GetComponentDataFromEntity<Health>(true),
            DamageEvent = GetBufferFromEntity<DamageEvent>()

        }.Schedule(stepPhysicsWorldSystem.Simulation, ref buildPhysicsWorldSystem.PhysicsWorld, Dependency);
    }
}