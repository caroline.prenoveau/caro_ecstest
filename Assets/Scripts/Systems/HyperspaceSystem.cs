﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

[UpdateBefore(typeof(BuildPhysicsWorld))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class HyperspaceSystem : SystemBase
{
    private BeginInitializationEntityCommandBufferSystem beginInitEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        beginInitEntityCommandBufferSystem = World.GetExistingSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var camera = Camera.main;

        float halfHeight = camera.orthographicSize + 1;
        float halfWidth = halfHeight * camera.aspect;

        EntityCommandBuffer ecbPackage = beginInitEntityCommandBufferSystem.CreateCommandBuffer();
        EntityCommandBuffer.ParallelWriter ecb = ecbPackage.AsParallelWriter();

        Entities.ForEach((
            Entity entity,
            int entityInQueryIndex,
            ref Translation trans,
            in Hyperspace hyperspace) =>
        {
            if (!InHyperspace(trans.Value, halfWidth, halfHeight))
                return;

            if(!hyperspace.wrapAround)
            {
                ecb.DestroyEntity(entityInQueryIndex, entity);
                return;
            }

            if (trans.Value.z < -halfHeight)
            {
                trans.Value.z = halfHeight;
            }
            else if (trans.Value.z > halfHeight)
            {
                trans.Value.z = -halfHeight;
            }

            if (trans.Value.x < -halfWidth)
            {
                trans.Value.x = halfWidth;
            }
            else if (trans.Value.x > halfWidth)
            {
                trans.Value.x = -halfWidth;
            }

        }).ScheduleParallel();

        beginInitEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

    public static bool InHyperspace(Vector3 position, float halfWidth, float halfHeight)
    {
        return position.x < -halfWidth || position.x > halfWidth || position.z < -halfHeight || position.z > halfHeight;
    }
}
