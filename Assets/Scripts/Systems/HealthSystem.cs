﻿using Unity.Entities;
using Unity.Jobs;

public class HealthSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        EntityCommandBuffer ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer();

        Entities.ForEach((Entity entity, ref Health health, ref DynamicBuffer<DamageEvent> damageEvents) =>
        {
            for (int i = 0; i < damageEvents.Length; i++)
            {
                health.amount -= damageEvents[i].damageAmount;

                if(health.amount <= 0)
                {
                    if (damageEvents[i].damageSource != Entity.Null
                    && HasComponent<Enemy>(entity)
                    && HasComponent<Player>(damageEvents[i].damageSource))
                    {
                        var player = GetComponent<Player>(damageEvents[i].damageSource);
                        player.score += GetComponent<Enemy>(entity).points;
                        SetComponent(damageEvents[i].damageSource, player);
                    }

                    if(health.autoDestruct)
                    {
                        ecb.DestroyEntity(entity);
                    }

                    break;
                }
            }
            damageEvents.Clear();

        }).Schedule();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
