using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

[System.Serializable]
public struct AsteroidSettings
{
    public AsteroidType type;
    public int health;
    public int points;
    public float spawnMinRadius;
    public float spawnMaxRadius;
    public float spawnSpeed;
    public int spawnCount;
}

[System.Serializable]
public struct AsteroidSpawnSettings
{
    public List<AsteroidSettings> asteroidSettings;

    public AsteroidSettings GetAsteroidSettings(AsteroidType type)
    {
        return asteroidSettings.Find(a => a.type == type);
    }
}

[AlwaysUpdateSystem]
public class AsteroidSpawnSystem : SystemBase
{
    private AsteroidSpawnSettings settings;
    private Entity bigAsteroidPrefab;
    private Entity medAsteroidPrefab;
    private Entity smallAsteroidPrefab;
    private bool started;

    private BeginInitializationEntityCommandBufferSystem beginInitEntityCommandBufferSystem;

    public void StartSystem(AsteroidSpawnSettings settings, Entity bigAsteroidPrefab, Entity medAsteroidPrefab, Entity smallAsteroidPrefab)
    {
        this.settings = settings;
        this.bigAsteroidPrefab = bigAsteroidPrefab;
        this.medAsteroidPrefab = medAsteroidPrefab;
        this.smallAsteroidPrefab = smallAsteroidPrefab;

        started = true;
    }

    public void StopSystem()
    {
        started = false;
    }

    protected override void OnCreate()
    {
        beginInitEntityCommandBufferSystem = World.GetExistingSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        if (!started)
            return;

        EntityQuery asteroids = GetEntityQuery(ComponentType.ReadOnly<Asteroid>());
        if (asteroids.CalculateEntityCount() == 0)
        {
            SpawnNewBatch();
        }
        else
        {
            using (var asteroidArray = GetEntityQuery(typeof(Asteroid)).ToEntityArray(Allocator.Temp))
            {
                foreach (Entity entity in asteroidArray)
                {
                    var health = EntityManager.GetComponentData<Health>(entity);
                    if(health.amount <= 0)
                    {
                        var asteroid = EntityManager.GetComponentData<Asteroid>(entity);
                        var trans = EntityManager.GetComponentData<Translation>(entity);

                        if (asteroid.type == AsteroidType.Big)
                        {
                            SpawnAsteroids(medAsteroidPrefab, settings.GetAsteroidSettings(AsteroidType.Medium), trans.Value);
                        }
                        else if (asteroid.type == AsteroidType.Medium)
                        {
                            SpawnAsteroids(smallAsteroidPrefab, settings.GetAsteroidSettings(AsteroidType.Small), trans.Value);
                        }
                    }
                }
            }

            EntityCommandBuffer ecbPackage = beginInitEntityCommandBufferSystem.CreateCommandBuffer();
            EntityCommandBuffer.ParallelWriter ecb = ecbPackage.AsParallelWriter();

            Entities.ForEach((
                Entity entity,
                int entityInQueryIndex,
                in Asteroid asteroid,
                in Health health,
                in Translation trans) =>
            {
                if (health.amount <= 0)
                {
                    ecb.DestroyEntity(entityInQueryIndex, entity);
                }

            }).ScheduleParallel();

            beginInitEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }

    private void SpawnNewBatch()
    {
        SpawnAsteroids(bigAsteroidPrefab, settings.GetAsteroidSettings(AsteroidType.Big), Vector3.zero);
    }

    private void SpawnAsteroids(Entity prefab, AsteroidSettings settings, Vector3 origin)
    {
        SpawnAsteroids(prefab, settings.spawnCount, settings.health, settings.points, settings.type, origin, settings.spawnMinRadius, settings.spawnMaxRadius, settings.spawnSpeed);
    }

    private void SpawnAsteroids(Entity prefab, int count, int health, int points, AsteroidType type, Vector3 origin, float spawnMinRadius, float spawnMaxRadius, float spawnSpeed)
    {
        var asteroids = new NativeArray<Entity>(count, Allocator.Temp);
        EntityManager.Instantiate(prefab, asteroids);

        EntityManager.AddComponent(asteroids, typeof(Asteroid));
        EntityManager.AddComponent(asteroids, typeof(Enemy));
        EntityManager.AddComponent(asteroids, typeof(Health));
        EntityManager.AddComponent(asteroids, typeof(DamageSource));

        foreach (Entity instance in asteroids)
        {
            SetAsteroidData(
                instance,
                health,
                points,
                type,
                GetRandomPosition(origin, spawnMinRadius, spawnMaxRadius),
                GetRandomRotation(),
                GetRandomDirection() * spawnSpeed);
        }

        asteroids.Dispose();
    }

    private void SetAsteroidData(Entity instance, int health, int points, AsteroidType type, Vector3 position, float rotation, Vector3 velocity)
    {
        EntityManager.AddBuffer<DamageEvent>(instance);

        EntityManager.SetComponentData(instance, new Asteroid
        {
            type = type
        });
        EntityManager.SetComponentData(instance, new Enemy
        {
            points = points
        });
        EntityManager.SetComponentData(instance, new Health
        {
            amount = health,
            maxAmount = health
        });
        EntityManager.SetComponentData(instance, new DamageSource
        {
            damageAmount = 1,
            sourceEntity = instance
        });
        EntityManager.SetComponentData(instance, new Translation
        {
            Value = position
        });
        EntityManager.SetComponentData(instance, new Rotation
        {
            Value = quaternion.RotateY(rotation)
        });
        EntityManager.SetComponentData(instance, new PhysicsVelocity
        {
            Linear = velocity
        });
    }

    public void DestroyAll()
    {
        EntityCommandBuffer ecbPackage = beginInitEntityCommandBufferSystem.CreateCommandBuffer();
        EntityCommandBuffer.ParallelWriter ecb = ecbPackage.AsParallelWriter();

        Entities.WithAll<Asteroid>().ForEach((Entity entity, int entityInQueryIndex) =>
        {
            ecb.DestroyEntity(entityInQueryIndex, entity);
        }).ScheduleParallel();

        beginInitEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

    public static Vector3 GetRandomPosition(Vector3 origin, float minRadius, float maxRadius)
    {
        Vector2 point = UnityEngine.Random.insideUnitCircle.normalized * UnityEngine.Random.Range(minRadius, maxRadius);
        return new Vector3(point.x, 0f, point.y) + origin;
    }

    public static float GetRandomRotation()
    {
        return UnityEngine.Random.Range(0, Mathf.PI * 2);
    }

    public static Vector3 GetRandomDirection()
    {
        Vector2 point = UnityEngine.Random.insideUnitCircle.normalized;
        return new Vector3(point.x, 0f, point.y);
    }
}
