﻿using HotChocolate.UI;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI
{
    public class GameHudData
    {
        public GameData gameData;
    }

    public class GameHud : MonoBehaviour, IMenu
    {
        [System.Serializable]
        public class PlayerInfo
        {
            public TMP_Text currentScoreText;
            public TMP_Text bestScoreText;
            public TMP_Text livesText;
        }

        public List<PlayerInfo> players = new List<PlayerInfo>();

        public string Id { get; set; }
        public bool HasFocus { get; set; }

        public Menu.BeforeFocusIn BeforeFocusIn => Menu.ActivateInstance;
        public Menu.AfterFocusOut AfterFocusOut => Menu.ActivateInstance;

        private GameHudData _data;

        public void OnPush(MenuStack stack, object data)
        {
            _data = data as GameHudData;

            _data.gameData.StateChanged -= Refresh;
            _data.gameData.StateChanged += Refresh;

            Refresh();
        }

        public void OnPop()
        {
            _data.gameData.StateChanged -= Refresh;
        }

        private void Refresh()
        {
            for (int i = 0; i < players.Count && i < _data.gameData.players.Count; ++i)
            {
                players[i].currentScoreText.text = "Score: " + _data.gameData.players[i].CurrentScore;
                players[i].bestScoreText.text = "Best: " + (_data.gameData.players[i].BestScore.HasValue ? _data.gameData.players[i].BestScore.Value.ToString() : "--");
                players[i].livesText.text = "Lives: " + _data.gameData.players[i].Lives;
            }
        }

        public void OnFocusIn() { }
        public void OnFocusOut() { }
    }
}
