﻿using HotChocolate.UI;
using UnityEngine;

namespace UI
{
    public class GameOverView : MonoBehaviour, IMenu
    {
        public string Id { get; set; }
        public bool HasFocus { get; set; }

        public Menu.BeforeFocusIn BeforeFocusIn => Menu.ActivateInstance;
        public Menu.AfterFocusOut AfterFocusOut => Menu.ActivateInstance;

        public void OnPush(MenuStack stack, object data) { }
        public void OnPop() { }
        public void OnFocusIn() { }
        public void OnFocusOut() { }
    }
}
