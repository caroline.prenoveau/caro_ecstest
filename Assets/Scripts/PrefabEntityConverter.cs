using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PrefabEntityConverter : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
{
    public GameObject playerShipPrefab;
    public GameObject bigAsteroidPrefab;
    public GameObject mediumAsteroidPrefab;
    public GameObject smallAsteroidPrefab;

    public static Entity playerShipEntity;
    public static Entity bigAsteroidEntity;
    public static Entity mediumAsteroidEntity;
    public static Entity smallAsteroidEntity;

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        referencedPrefabs.Add(playerShipPrefab);
        referencedPrefabs.Add(bigAsteroidPrefab);
        referencedPrefabs.Add(mediumAsteroidPrefab);
        referencedPrefabs.Add(smallAsteroidPrefab);
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        playerShipEntity = conversionSystem.GetPrimaryEntity(playerShipPrefab);
        bigAsteroidEntity = conversionSystem.GetPrimaryEntity(bigAsteroidPrefab);
        mediumAsteroidEntity = conversionSystem.GetPrimaryEntity(mediumAsteroidPrefab);
        smallAsteroidEntity = conversionSystem.GetPrimaryEntity(smallAsteroidPrefab);
    }
}
