﻿using Unity.Entities;

public struct Health : IComponentData
{
    public int amount;
    public int maxAmount;
    public bool autoDestruct;
}

public struct DamageEvent : IBufferElementData
{
    public int damageAmount;
    public Entity damageSource;
}