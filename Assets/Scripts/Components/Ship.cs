﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Ship : IComponentData
{
    public Entity bulletPrefab;
    public Entity bulletSpawnPoint;

    public float rotationSpeed;
    public float thrustForce;
    public float bulletSpeed;

    [HideInInspector] public bool rotateLeft;
    [HideInInspector] public bool rotateRight;
    [HideInInspector] public bool thrust;
    [HideInInspector] public bool shootTrigger;
}
