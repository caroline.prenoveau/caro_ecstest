﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct Hyperspace : IComponentData
{
    public bool wrapAround;
}
