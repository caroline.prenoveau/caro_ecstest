﻿using Unity.Entities;

public enum AsteroidType
{
    Big,
    Medium,
    Small
}

public struct Asteroid : IComponentData
{
    public AsteroidType type;
}
