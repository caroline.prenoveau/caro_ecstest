﻿using Unity.Entities;
using UnityEngine;

[System.Serializable]
public struct PlayerInputs
{
    public KeyCode rotateLeft;
    public KeyCode rotateRight;
    public KeyCode thrust;
    public KeyCode shoot;
}

public struct Player : IComponentData
{
    public int playerIndex;
    public PlayerInputs inputs;
    public int lives;
    public int score;
    public Entity ship;
}
