﻿using Unity.Entities;

public struct DamageSource : IComponentData
{
    public int damageAmount;
    public Entity sourceEntity;
}
