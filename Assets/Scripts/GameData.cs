﻿using System;
using System.Collections.Generic;

public class PlayerData
{
    public event Action StateChanged;

    public int? BestScore
    {
        get { return bestScore; }
        set
        {
            if (bestScore != value)
            {
                bestScore = value;
                StateChanged?.Invoke();
            }
        }
    }
    private int? bestScore;

    public int CurrentScore
    {
        get { return currentScore; }
        set
        {
            if (currentScore != value)
            {
                currentScore = value;
                StateChanged?.Invoke();
            }
        }
    }
    private int currentScore;

    public int Lives
    {
        get { return lives; }
        set
        {
            if (lives != value)
            {
                lives = value;
                StateChanged?.Invoke();
            }
        }
    }
    private int lives;
}

public class GameData
{
    public List<PlayerData> players = new List<PlayerData>();

    public event Action StateChanged;

    public GameData(int playerCount)
    {
        for(int i = 0; i < playerCount; ++i)
        {
            var player = new PlayerData();
            player.StateChanged += () => { StateChanged?.Invoke(); };
            players.Add(player);
        }
    }
}