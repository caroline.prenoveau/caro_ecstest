﻿using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "MenuDefinition", menuName = "Menu Definition", order = 1)]
public class MenuDefinition : ScriptableObject
{
    public GameObject prefabRef;
    public string id;
}