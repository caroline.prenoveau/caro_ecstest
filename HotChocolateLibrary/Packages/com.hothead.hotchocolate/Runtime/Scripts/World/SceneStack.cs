﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using System.Threading;

namespace HotChocolate.World
{
    public sealed class SceneStack : MonoBehaviour
    {
        public GameObject inputBlocker;

        public int Count => _stack.Count;
        public ISceneData Find(string id) => _stack.Find(m => m.SceneDef.SceneName == id);
        public ISceneData Top => _stack.Count > 0 ? _stack.Last() : null;

        public delegate void ScenePushed(ISceneData instance);
        public event ScenePushed OnScenePushed;

        public delegate void ScenePopped(ISceneData instance);
        public event ScenePopped OnScenePopped;

        public Scene.Load loadSceneDefault = Scene.LoadSceneDefault;
        public Scene.Unload unloadSceneDefault = Scene.UnloadSceneDefault;

        public Scene.BeforeFocusIn beforeFocusIn = Scene.ActivateScene;
        public Scene.AfterFocusOut afterFocusOut = Scene.DeactivateScene;

        private List<ISceneData> _stack = new List<ISceneData>();
        private CancellationTokenSource _ctSource = new CancellationTokenSource();

        private void OnDestroy()
        {
            _ctSource.Cancel();
        }

        public async Task Push(ISceneDefinition sceneDef, LoadSceneMode loadSceneMode)
        {
            await Push(sceneDef, loadSceneDefault, loadSceneMode).ConfigureAwait(true);
        }

        public async Task Push(ISceneDefinition sceneDef, Scene.Load loadSceneTask, LoadSceneMode loadSceneMode)
        {
            if (Find(sceneDef.SceneName) != null)
            {
                Debug.LogWarning("Push: scene with name " + sceneDef.SceneName + " already exists in the stack. Call PopAllAbove instead.");
                return;
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            if (_stack.Count > 0)
            {
                afterFocusOut?.Invoke(Top);
            }

            var instance = await loadSceneTask(sceneDef, loadSceneMode, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            _stack.Add(instance);

            OnScenePushed?.Invoke(instance);
            beforeFocusIn?.Invoke(instance);

            if (inputBlocker != null)
                inputBlocker.SetActive(false);
        }

        public async Task Jump(ISceneDefinition sceneDef, string fromSceneName, LoadSceneMode loadSceneMode)
        {
            await Jump(sceneDef, fromSceneName, unloadSceneDefault, loadSceneDefault, loadSceneMode).ConfigureAwait(true);
        }

        public async Task Jump(ISceneDefinition sceneDef, string fromSceneName, Scene.Unload unloadSceneTask, Scene.Load loadSceneTask, LoadSceneMode loadSceneMode)
        {
            if (Find(sceneDef.SceneName) != null)
            {
                await PopAllAbove(sceneDef.SceneName, unloadSceneTask, loadSceneTask).ConfigureAwait(true);
                return;
            }

            if (string.IsNullOrEmpty(fromSceneName))
            {
                await Jump(sceneDef, unloadSceneTask, loadSceneTask).ConfigureAwait(true);
                return;
            }

            if (Top != null && Top.SceneDef.SceneName == fromSceneName)
            {
                await Push(sceneDef, loadSceneTask, loadSceneMode).ConfigureAwait(true);
                return;
            }

            var targetMenuIndex = _stack.FindIndex(m => m.SceneDef.SceneName == fromSceneName);
            if (targetMenuIndex == -1)
            {
                Debug.LogWarning("Jump: scene with name " + fromSceneName + " doesnt exist in the stack");
                return;
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            var instance = await loadSceneTask(sceneDef, loadSceneMode, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            if (loadSceneMode == LoadSceneMode.Additive)
                Scene.DeactivateScene(instance);

            int startIndex = _stack.Count - 1;
            for (int i = startIndex; i > targetMenuIndex; --i)
            {
                await Pop(unloadSceneTask, loadSceneTask, focusOut: i == startIndex, focusIn: false).ConfigureAwait(true);
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            _stack.Add(instance);
            Scene.ActivateScene(instance);

            OnScenePushed?.Invoke(instance);
            beforeFocusIn?.Invoke(instance);

            if (inputBlocker != null)
                inputBlocker.SetActive(false);
        }

        private async Task Jump(ISceneDefinition sceneDef, Scene.Unload unloadSceneTask, Scene.Load loadSceneTask)
        {
            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            var instance = await loadSceneTask(sceneDef, LoadSceneMode.Single, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            await PopAll(unloadSceneTask, loadSceneTask, focusIn: false).ConfigureAwait(true);

            var toPop = Top;
            OnScenePopped?.Invoke(toPop);
            _stack.Clear();

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            _stack.Add(instance);

            OnScenePushed?.Invoke(instance);
            beforeFocusIn?.Invoke(instance);

            if (inputBlocker != null)
                inputBlocker.SetActive(false);
        }

        public async Task Pop()
        {
            await Pop(unloadSceneDefault, loadSceneDefault, true , true).ConfigureAwait(true);
        }

        public async Task Pop(Scene.Unload unloadSceneTask, Scene.Load loadSceneTask)
        {
            await Pop(unloadSceneDefault, loadSceneDefault, true, true).ConfigureAwait(true);
        }

        private async Task Pop(Scene.Unload unloadSceneTask, Scene.Load loadSceneTask, bool focusOut, bool focusIn)
        {
            if (_stack.Count <= 1)
            {
                return;
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            if (focusOut)
            {
                afterFocusOut?.Invoke(Top);
            }

            var toPop = Top;
            _stack.RemoveAt(_stack.Count - 1);

            OnScenePopped?.Invoke(toPop);
            await unloadSceneTask(toPop, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            if (_stack.Count > 0 && focusIn)
            {
                if (!Top.Scene.isLoaded)
                {
                    var instance = await loadSceneTask(Top.SceneDef, LoadSceneMode.Single, this, _ctSource.Token).ConfigureAwait(true);

                    if (_ctSource.IsCancellationRequested)
                        return;

                    _stack[_stack.Count - 1] = instance;
                }

                beforeFocusIn?.Invoke(Top);
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(false);
        }

        public async Task PopAllAbove(string sceneName)
        {
            await PopAllAbove(sceneName, unloadSceneDefault, loadSceneDefault).ConfigureAwait(true);
        }

        public async Task PopAllAbove(string sceneName, Scene.Unload unloadSceneTask, Scene.Load loadSceneTask)
        {
            var targetMenuIndex = _stack.FindIndex(m => m.SceneDef.SceneName == sceneName);
            if (targetMenuIndex == -1)
            {
                Debug.LogWarning("PopAllAbove: scene with name " + sceneName + " doesnt exist in the stack");
                return;
            }

            int startIndex = _stack.Count - 1;
            for (int i = startIndex; i > targetMenuIndex; --i)
            {
                await Pop(unloadSceneTask, loadSceneTask, focusOut: i == startIndex, focusIn: i == targetMenuIndex + 1).ConfigureAwait(true);
            }
        }

        public async Task PopAll()
        {
            await PopAll(unloadSceneDefault, loadSceneDefault).ConfigureAwait(true);
        }

        public async Task PopAll(Scene.Unload unloadSceneTask, Scene.Load loadSceneTask)
        {
            await PopAll(unloadSceneTask, loadSceneTask, true).ConfigureAwait(true);
        }

        private async Task PopAll(Scene.Unload unloadSceneTask, Scene.Load loadSceneTask, bool focusIn)
        {
            await Pop(unloadSceneTask, loadSceneTask, focusOut: true, focusIn: focusIn && _stack.Count == 2).ConfigureAwait(true);

            while (_stack.Count > 1)
            {
                await Pop(unloadSceneTask, loadSceneTask, focusOut: false, focusIn: focusIn && _stack.Count == 2).ConfigureAwait(true);
            }
        }
    }
}