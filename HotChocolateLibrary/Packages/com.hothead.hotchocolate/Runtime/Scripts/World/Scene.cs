﻿using HotChocolate.Utils;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HotChocolate.World
{
    public interface ISceneDefinition
    {
        string SceneName { get; }
    }

    public interface ISceneData
    {
        ISceneDefinition SceneDef { get; }
        UnityEngine.SceneManagement.Scene Scene { get; }
    }

    public class DefaultSceneData : ISceneData
    {
        public ISceneDefinition SceneDef { get; set; }
        public UnityEngine.SceneManagement.Scene Scene => SceneManager.GetSceneByName(SceneNameFromPath(SceneDef.SceneName));

        public static string SceneNameFromPath(string scenePath)
        {
            if (string.IsNullOrEmpty(scenePath))
                return null;

            var tokens = scenePath.Split('/');
            if (tokens.Length > 0)
            {
                var filename = tokens.Last();
                return filename.Split('.')[0];
            }

            return null;
        }
    }

    public static partial class Scene
    {
        public delegate Task<ISceneData> Load(ISceneDefinition sceneDefinition, LoadSceneMode loadSceneMode, MonoBehaviour opHolder, CancellationToken ct);
        public delegate Task Unload(ISceneData instance, MonoBehaviour opHolder, CancellationToken ct);

        public delegate void BeforeFocusIn(ISceneData instance);
        public delegate void AfterFocusOut(ISceneData instance);

        public static async Task<ISceneData> LoadSceneDefault(ISceneDefinition sceneDef, LoadSceneMode loadSceneMode, MonoBehaviour opHolder, CancellationToken ct)
        {
            if (!ct.IsCancellationRequested)
            {
                var op = SceneManager.LoadSceneAsync(sceneDef.SceneName, loadSceneMode);
                await opHolder.StartCoroutineAsync(op, ct).ConfigureAwait(true);
            }

            var sceneData = new DefaultSceneData
            {
                SceneDef = sceneDef
            };

            return sceneData;
        }

        public static async Task UnloadSceneDefault(ISceneData sceneData, MonoBehaviour opHolder, CancellationToken ct)
        {
            if (!ct.IsCancellationRequested && sceneData.Scene.isLoaded && sceneData.Scene.buildIndex != -1)
            {
                var op = SceneManager.UnloadSceneAsync(sceneData.SceneDef.SceneName);
                await opHolder.StartCoroutineAsync(op, ct).ConfigureAwait(true);
            }
        }

        public static void ActivateScene(ISceneData sceneData)
        {
            if(sceneData.Scene.IsValid())
            {
                var children = sceneData.Scene.GetRootGameObjects();
                foreach(var child in children)
                {
                    child.SetActive(true);
                }
            }
        }

        public static void DeactivateScene(ISceneData sceneData)
        {
            if (sceneData.Scene.IsValid())
            {
                var children = sceneData.Scene.GetRootGameObjects();
                foreach (var child in children)
                {
                    child.SetActive(false);
                }
            }
        }
    }
}
