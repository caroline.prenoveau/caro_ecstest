﻿using HotChocolate.Utils;
using SoftMasking;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace HotChocolate.FTUE.Widgets
{
    public class TutorialView<TData, TResult> : MonoBehaviour, IFtueElement where TData : class where TResult : IFtueResult
    {
        public GameObject blocker;
        public GameObject tutorialPanel;

        public RectTransform pointerWorldRect;
        public RectTransform hintBoxAnchor;
        public RectTransform dialogueBoxAnchor;
        public RectTransform pointerAnchor;
        public Button continueButton;
        public SoftMask mask;

        public delegate Camera GetCameraDelegate(TData data);
        public GetCameraDelegate getCamera;

        public delegate Task RecenterCameraDelegate(IWorldHint worldHint, TData data, CancellationToken ct);
        public RecenterCameraDelegate recenterCamera;

        public delegate Transform FindMenuRootDelegate(IMenuHint menuHint, TData data);
        public FindMenuRootDelegate findMenuRoot;

        private bool done;

        private TData _data;
        private IFtueListener _listener;

        private void Awake()
        {
            continueButton.onClick.AddListener(ContinueButtonClicked);

            blocker.SetActive(false);
            tutorialPanel.SetActive(false);

            DontDestroyOnLoad(gameObject);
        }

        public void Init(TData data, IFtueListener listener, GetCameraDelegate getCamera, RecenterCameraDelegate recenterCamera, FindMenuRootDelegate findMenuRoot)
        {
            _data = data;
            _listener = listener;

            this.getCamera = getCamera;
            this.recenterCamera = recenterCamera;
            this.findMenuRoot = findMenuRoot;

            _listener.RegisterElement(this);
        }

        private void OnDestroy()
        {
            if(_listener != null)
                _listener.UnregisterElement(this);
        }

        public bool ShouldActivateFtue(IFtueStep step)
        {
            return step is IWorldHint || step is IMenuHint || step is IDialogue;
        }

        public async Task ActivateFtue(IFtueStep step, CancellationToken ct)
        {
            done = false;

            if (step is IWorldHint worldHint)
            {
                await ActivateWorldFtue(worldHint, ct).ConfigureAwait(true);
            }
            else if (step is IMenuHint menuHint)
            {
                await ActivateMenuFtue(menuHint, ct).ConfigureAwait(true);
            }
            else if (step is IDialogue dialogue)
            {
                await ActivateDialogueFtue(dialogue, ct).ConfigureAwait(true);
            }
        }

        private async Task ActivateWorldFtue(IWorldHint worldHint, CancellationToken ct)
        {
            blocker.SetActive(true);
            continueButton.gameObject.SetActive(false);

            if (worldHint.RecenterCamera)
            {
                await recenterCamera(worldHint, _data, ct).ConfigureAwait(true);
            }

            var camera = getCamera?.Invoke(_data);
            if (camera != null)
            {
                var vertices = GetVertices(worldHint, camera);

                var minMax = Positions.FindViewMinMax(vertices, camera);

                pointerWorldRect.GetComponent<RectTransform>().anchorMin = minMax.Item1;
                pointerWorldRect.GetComponent<RectTransform>().anchorMax = minMax.Item2;

                var pointerInstance = Instantiate(worldHint.Config.pointerPrefab, pointerAnchor);
                pointerInstance.Init(pointerWorldRect, pointerWorldRect.GetComponentInParent<Canvas>().worldCamera, worldHint.PointerAnchor, worldHint.Completion == WorldHintCompletion.TapAnywhere);

                TutorialHintBox hintBoxInstance = null;
                if (worldHint.Config.hintBoxPrefab != null && !string.IsNullOrEmpty(worldHint.Hint))
                {
                    hintBoxInstance = Instantiate(worldHint.Config.hintBoxPrefab, hintBoxAnchor);
                    hintBoxInstance.Init(pointerInstance.handAnchor, null, worldHint.HintBoxAnchor, worldHint.Hint);
                }

                var dialogBoxInstance = dialogueBoxAnchor.GetComponentInChildren<TutorialDialogueBox>();
                if (worldHint.Config.dialogueBoxPrefab != null && !string.IsNullOrEmpty(worldHint.Dialogue))
                {
                    if (dialogBoxInstance == null)
                    {
                        dialogBoxInstance = Instantiate(worldHint.Config.dialogueBoxPrefab, dialogueBoxAnchor);
                    }
                    else if (dialogBoxInstance.name != worldHint.Config.dialogueBoxPrefab.name + "(Clone)")
                    {
                        Destroy(dialogBoxInstance.gameObject);
                        dialogBoxInstance = Instantiate(worldHint.Config.dialogueBoxPrefab, dialogueBoxAnchor);
                        dialogBoxInstance.AnimationPlayed = true;
                    }

                    dialogBoxInstance.Init(worldHint.DialogueBubbleAnchor, worldHint.DialogueCharacterAnchor, worldHint.Dialogue, worldHint.CharacterName, worldHint.CharacterSprite);
                }

                if (mask != null)
                    mask.separateMask = pointerInstance.maskRect;

                if (worldHint.Highlightable != null)
                    worldHint.Highlightable.StartHighlight();

                tutorialPanel.SetActive(true);
                await PlayPushAnimation(dialogBoxInstance, hintBoxInstance, pointerInstance);

                continueButton.gameObject.SetActive(worldHint.Completion != WorldHintCompletion.WaitForCancel);

                while (!done && !ct.IsCancellationRequested)
                {
                    await Task.Yield();

                    minMax = Positions.FindViewMinMax(vertices, camera);

                    pointerWorldRect.GetComponent<RectTransform>().anchorMin = minMax.Item1;
                    pointerWorldRect.GetComponent<RectTransform>().anchorMax = minMax.Item2;

                    pointerInstance.UpdatePosition();
                    if (hintBoxInstance != null) hintBoxInstance.UpdatePosition();
                }

                await PlayPopAnimation(dialogBoxInstance, hintBoxInstance, pointerInstance, worldHint.DontPopDialogueBox);

                if (worldHint.Highlightable != null)
                    worldHint.Highlightable.StopHighlight();
            }

            tutorialPanel.SetActive(false);
            blocker.SetActive(false);
        }

        private async Task ActivateMenuFtue(IMenuHint menuHint, CancellationToken ct)
        {
            blocker.SetActive(true);

            Button button = null;
            ScrollRect scrollRect = null;
            bool scrollRectWasEnabled = true;

            continueButton.gameObject.SetActive(false);
            blocker.SetActive(menuHint.Completion == MenuHintCompletion.TapAnywhere);

            var currentMenuElement = await FindElementAsync(menuHint, ct).ConfigureAwait(true);

            if (currentMenuElement == null || (menuHint.SkipIfInactive && !currentMenuElement.gameObject.activeInHierarchy))
            {
                Debug.LogWarning("Menu Element with Tag ID " + menuHint.TagId + " not found! Exiting step.");
                done = true;
            }
            else
            {
                void UpdateButton()
                {
                    if (menuHint.Completion == MenuHintCompletion.PressButton)
                    {
                        button = currentMenuElement.GetComponentInChildren<Button>(true);
                        if (button != null)
                        {
                            button.onClick.AddListener(ContinueButtonClicked);

                            scrollRect = button.GetComponentInParent<ScrollRect>();
                            if (scrollRect != null)
                            {
                                scrollRectWasEnabled = scrollRect.enabled;
                                scrollRect.enabled = false;
                            }
                        }
                        else
                        {
                            Debug.LogWarning("Menu Element with Tag ID " + menuHint.TagId + " did not contain button! Exiting step.");
                            done = true;
                        }
                    }
                }

                UpdateButton();

                if (!done)
                {
                    var menuRoot = findMenuRoot(menuHint, _data);
                    var canvas = menuRoot != null ? menuRoot.GetComponent<Canvas>() : null;

                    var pointerInstance = Instantiate(menuHint.Config.pointerPrefab, pointerAnchor);

                    void InitPointer()
                    {
                        var rectTransform = currentMenuElement.GetComponent<RectTransform>();
                        pointerInstance.Init(rectTransform, canvas != null ? canvas.worldCamera : null, menuHint.PointerAnchor, menuHint.Completion == MenuHintCompletion.TapAnywhere);
                    }
                    InitPointer();

                    TutorialHintBox hintBoxInstance = null;
                    if (menuHint.Config.hintBoxPrefab != null && !string.IsNullOrEmpty(menuHint.Hint))
                    {
                        hintBoxInstance = Instantiate(menuHint.Config.hintBoxPrefab, hintBoxAnchor);
                        hintBoxInstance.Init(pointerInstance.handAnchor, null, menuHint.HintBoxAnchor, menuHint.Hint);
                    }

                    var dialogBoxInstance = dialogueBoxAnchor.GetComponentInChildren<TutorialDialogueBox>();
                    if (menuHint.Config.dialogueBoxPrefab != null && !string.IsNullOrEmpty(menuHint.Dialogue))
                    {
                        if (dialogBoxInstance == null)
                        {
                            dialogBoxInstance = Instantiate(menuHint.Config.dialogueBoxPrefab, dialogueBoxAnchor);
                        }
                        else if (dialogBoxInstance.name != menuHint.Config.dialogueBoxPrefab.name + "(Clone)")
                        {
                            Destroy(dialogBoxInstance.gameObject);
                            dialogBoxInstance = Instantiate(menuHint.Config.dialogueBoxPrefab, dialogueBoxAnchor);
                            dialogBoxInstance.AnimationPlayed = true;
                        }

                        dialogBoxInstance.Init(menuHint.DialogueBubbleAnchor, menuHint.DialogueCharacterAnchor, menuHint.Dialogue, menuHint.CharacterName, menuHint.CharacterSprite);
                    }

                    if (mask != null)
                        mask.separateMask = pointerInstance.maskRect;

                    IHighlightable highlightable = null;
                    if (menuHint.HighlightElement)
                    {
                        highlightable = currentMenuElement.GetComponentInChildren<IHighlightable>();

                        if (highlightable != null)
                            highlightable.StartHighlight();
                    }

                    tutorialPanel.SetActive(true);
                    await PlayPushAnimation(dialogBoxInstance, hintBoxInstance, pointerInstance);

                    continueButton.gameObject.SetActive(menuHint.Completion == MenuHintCompletion.TapAnywhere);

                    while (!done && !ct.IsCancellationRequested)
                    {
                        pointerInstance.UpdatePosition();
                        if (hintBoxInstance != null) hintBoxInstance.UpdatePosition();

                        await Task.Yield();

                        if (currentMenuElement == null)
                        {
                            blocker.SetActive(true);

                            currentMenuElement = await FindElementAsync(menuHint, ct).ConfigureAwait(true);
                            if (currentMenuElement != null)
                            {
                                UpdateButton();
                                InitPointer();

                                blocker.SetActive(menuHint.Completion == MenuHintCompletion.TapAnywhere);
                            }
                            else
                            {
                                Debug.LogWarning("Menu Element with Tag ID " + menuHint.TagId + " not found! Exiting step.");
                                done = true;
                            }
                        }
                    }

                    await PlayPopAnimation(dialogBoxInstance, hintBoxInstance, pointerInstance, menuHint.DontPopDialogueBox);

                    if (highlightable != null)
                        highlightable.StopHighlight();
                }
            }

            if (button != null)
            {
                button.onClick.RemoveListener(ContinueButtonClicked);

                if (scrollRect != null)
                    scrollRect.enabled = scrollRectWasEnabled;
            }

            tutorialPanel.SetActive(false);
            blocker.SetActive(false);
        }

        private async Task ActivateDialogueFtue(IDialogue dialogue, CancellationToken ct)
        {
            continueButton.gameObject.SetActive(false);
            blocker.SetActive(true);

            if (mask != null)
                mask.gameObject.SetActive(false);

            var dialogBoxInstance = dialogueBoxAnchor.GetComponentInChildren<TutorialDialogueBox>();

            if (dialogBoxInstance == null)
            {
                dialogBoxInstance = Instantiate(dialogue.Config.dialogueBoxPrefab, dialogueBoxAnchor);
            }
            else if (dialogBoxInstance.name != dialogue.Config.dialogueBoxPrefab.name + "(Clone)")
            {
                Destroy(dialogBoxInstance.gameObject);
                dialogBoxInstance = Instantiate(dialogue.Config.dialogueBoxPrefab, dialogueBoxAnchor);
                dialogBoxInstance.AnimationPlayed = true;
            }

            dialogBoxInstance.Init(dialogue.DialogueBubbleAnchor, dialogue.DialogueCharacterAnchor, dialogue.Dialogue, dialogue.CharacterName, dialogue.CharacterSprite);

            tutorialPanel.SetActive(true);
            await PlayPushAnimation(dialogBoxInstance, null, null);

            continueButton.gameObject.SetActive(true);

            while (!done && !ct.IsCancellationRequested)
            {
                await Task.Yield();
            }

            await PlayPopAnimation(dialogBoxInstance, null, null, dialogue.DontPopDialogueBox);

            tutorialPanel.SetActive(false);
            blocker.SetActive(false);

            if (mask != null)
                mask.gameObject.SetActive(true);
        }

        private async Task UpdatePosition(TutorialPointer pointerInstance, TutorialHintBox hintBoxInstance, CancellationToken ct)
        {
            while (!ct.IsCancellationRequested)
            {
                if (pointerInstance != null) pointerInstance.UpdatePosition();
                if (hintBoxInstance != null) hintBoxInstance.UpdatePosition();

                await Task.Yield();
            }
        }

        private async Task PlayPushAnimation(TutorialDialogueBox dialogBoxInstance, TutorialHintBox hintBoxInstance, TutorialPointer pointerInstance)
        {
            List<Task> tasks = new List<Task>();

            if (dialogBoxInstance != null)
                tasks.Add(dialogBoxInstance.Push());

            if (hintBoxInstance != null)
                tasks.Add(hintBoxInstance.Push());

            if (pointerInstance != null)
                tasks.Add(pointerInstance.Push());

            var ctSource = new CancellationTokenSource();
            UpdatePosition(pointerInstance, hintBoxInstance, ctSource.Token).FireAndForgetTask();

            await Task.WhenAll(tasks).ConfigureAwait(true);

            ctSource.Cancel();
            ctSource.Dispose();
        }

        private async Task PlayPopAnimation(TutorialDialogueBox dialogBoxInstance, TutorialHintBox hintBoxInstance, TutorialPointer pointerInstance, bool dontPopDialogueBox)
        {
            List<Task> tasks = new List<Task>();

            if (dialogBoxInstance != null && !dontPopDialogueBox)
                tasks.Add(dialogBoxInstance.Pop());

            if (hintBoxInstance != null)
                tasks.Add(hintBoxInstance.Pop());

            if (pointerInstance != null)
                tasks.Add(pointerInstance.Pop());

            await Task.WhenAll(tasks).ConfigureAwait(true);
            await Task.Yield();
            await Task.Yield();
        }

        private async Task<Transform> FindElementAsync(IMenuHint menuHint, CancellationToken ct)
        {
            Transform currentMenuElement = null;

            if (string.IsNullOrEmpty(menuHint.TagId))
            {
                return tutorialPanel.transform;
            }

            var menuRoot = findMenuRoot(menuHint, _data);
            if(!string.IsNullOrEmpty(menuHint.SearchParent))
            {
                menuRoot = FindByName(menuRoot, menuHint.SearchParent);
            }

            float totalTime = 0;
            while (currentMenuElement == null && totalTime < menuHint.SearchTimeout && !ct.IsCancellationRequested)
            {
                currentMenuElement = FindByTag(menuRoot, menuHint.TagId, menuHint.IncludeInactive);

                if (currentMenuElement == null)
                {
                    await Task.Delay(500).ConfigureAwait(true);
                    totalTime += 0.5f;
                }
            }

            return currentMenuElement;
        }

        private Transform FindByName(Transform parent, string name)
        {
            if (parent == null)
                return null;

            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(parent);

            while (queue.Count > 0)
            {
                var element = queue.Dequeue();
                if (element.name == name)
                {
                    return element;
                }

                foreach (Transform child in element)
                {
                    queue.Enqueue(child);
                }
            }

            return null;
        }

        private Transform FindByTag(Transform parent, string id, bool includeInactive)
        {
            if (parent == null)
                return null;

            var tags = parent.GetComponentsInChildren<FtueTag>(includeInactive);
            var tag = tags.FirstOrDefault(t => t.id == id);

            return tag != null ? tag.transform : null;
        }

        private Vector3[] GetVertices(IWorldHint worldHint, Camera camera)
        {
            if (worldHint.Collider != null)
            {
                return Positions.GetVertices(worldHint.Collider.bounds);
            }
            else
            {
                Vector3[] vertices = new Vector3[2];
                vertices[0] = worldHint.Position - camera.transform.right * worldHint.Radius - camera.transform.up * worldHint.Radius;
                vertices[1] = worldHint.Position + camera.transform.right * worldHint.Radius + camera.transform.up * worldHint.Radius;

                return vertices;
            }
        }

        private void ContinueButtonClicked()
        {
            done = true;
        }
    }
}
