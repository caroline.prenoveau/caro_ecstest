﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Threading.Tasks;
using HotChocolate.Utils;
using System.Threading;

namespace HotChocolate.UI
{
    public sealed class MenuStack : MonoBehaviour
    {
        public RectTransform menuAnchor;
        public GameObject inputBlocker;

        public int Count => _stack.Count;
        public IMenu Find(string id) => _stack.Find(m => m.Id == id);
        public IMenu Top => _stack.Count > 0 ? _stack.Last() : null;
        public int PendingCount => _actionQueue.Count;

        public delegate void MenuPushed(IMenu instance);
        public event MenuPushed OnMenuPushed;

        public delegate void MenuPopped(IMenu instance);
        public event MenuPopped OnMenuPopped;

        public delegate void MenuFocusChanged(IMenu instance, bool hasFocus);
        public event MenuFocusChanged OnMenuFocusChanged;

        public Menu.CreateInstance createInstanceDefault = Menu.CreateInstanceFromPrefab;
        public Menu.DestroyInstance destroyInstanceDefault = Menu.DestroyPrefabInstance;

        public Menu.PlayInAnimation playPushAnimationDefault = Menu.PlayPushAnimationDefault;
        public Menu.PlayOutAnimation playPopAnimationDefault = Menu.PlayPopAnimationDefault;
        public Menu.PlayInAnimation playFocusInAnimationDefault = Menu.PlayFocusInAnimationDefault;
        public Menu.PlayOutAnimation playFocusOutAnimationDefault = Menu.NoAnimation;

        private List<IMenu> _stack = new List<IMenu>();
        private Queue<Action> _actionQueue = new Queue<Action>();

        private CancellationTokenSource _ctSource = new CancellationTokenSource();

        private void Awake()
        {
            if (inputBlocker != null)
                inputBlocker.SetActive(false);
        }

        private void OnDestroy()
        {
            _ctSource.Cancel();
        }

        /// <summary>
        /// Instantiates a new menu using the default createInstance function and adds it on top of the stack.
        /// Will fail if the menuId already exists in the stack.
        /// </summary>
        /// <param name="menuAsset">The prefab or addressable key to instantiate</param>
        /// <param name="menuId">The id to be associated to the new instance</param>
        /// <param name="data">Anything you want to pass to the new menu instance</param>
        public void Push(object menuAsset, string menuId, object data = null)
        {
            Push(menuAsset, menuId, createInstanceDefault, data);
        }

        /// <summary>
        /// Instantiates a new menu using the createInstanceTask and adds it on top of the stack.
        /// Will fail if the menuId already exists in the stack.
        /// </summary>
        /// <param name="menuAsset">The prefab or addressable key to instantiate</param>
        /// <param name="menuId">The id to be associated to the new instance</param>
        /// <param name="createInstanceTask">The function to use to instantiate this menu</param>
        /// <param name="data">Anything you want to pass to the new menu instance</param>
        public void Push(object menuAsset, string menuId, Menu.CreateInstance createInstanceTask, object data = null)
        {
            Push(menuAsset, menuId, playFocusOutAnimationDefault, createInstanceTask, playPushAnimationDefault, data);
        }

        /// <summary>
        /// Instantiates a new menu using the createInstanceTask and adds it on top of the stack.
        /// Will fail if the menuId already exists in the stack.
        /// </summary>
        /// <param name="menuAsset">The prefab or addressable key to instantiate</param>
        /// <param name="menuId">The id to be associated to the new instance</param>
        /// <param name="playFocusOutAnimationTask">The animation function to play on the menu that would focus out</param>
        /// <param name="createInstanceTask">The function used to instantiate this menu</param>
        /// <param name="playPushAnimationTask">The animation function to play on the menu that will be pushed</param>
        /// <param name="data">Anything you want to pass to the new menu instance</param>
        public void Push(object menuAsset, string menuId, Menu.PlayOutAnimation playFocusOutAnimationTask, Menu.CreateInstance createInstanceTask, Menu.PlayInAnimation playPushAnimationTask, object data = null)
        {
            QueueAction(() => { DoPush(menuAsset, menuId, playFocusOutAnimationTask, createInstanceTask, playPushAnimationTask, data, ExecuteNextAction).FireAndForgetTask(); });
        }

        /// <summary>
        /// If fromMenu is not null or empty, pops all above fromMenu and pushes new menu.
        /// If fromMenu is null or empty, pops all and pushes new menu.
        /// If the menuId already exists in the stack, pops all above it.
        /// Fails if fromMenu is not null or empty and doesn't exist in the stack.
        /// </summary>
        /// <param name="menuAsset">The prefab or addressable key to instantiate</param>
        /// <param name="menuId">The id to be associated to this instance</param>
        /// <param name="fromMenu">The id of the menu to jump from</param>
        /// <param name="data">Anything you want to pass to the new menu instance</param>
        public void Jump(object menuAsset, string menuId, string fromMenu, object data = null)
        {
            Jump(menuAsset, menuId, fromMenu, createInstanceDefault, data);
        }

        /// <summary>
        /// If fromMenu is not null or empty, pops all above fromMenu and pushes new menu.
        /// If fromMenu is null or empty, pops all and pushes new menu.
        /// If the menuId already exists in the stack, pops all above it.
        /// Fails if fromMenu is not null or empty and doesn't exist in the stack.
        /// </summary>
        /// <param name="menuAsset">The prefab or addressable key to instantiate</param>
        /// <param name="menuId">The id to be associated to this instance</param>
        /// <param name="fromMenu">The id of the menu to jump from</param>
        /// <param name="createInstanceTask">The function to use to instantiate this menu</param>
        /// <param name="data">Anything you want to pass to the new menu instance</param>
        public void Jump(object menuAsset, string menuId, string fromMenu, Menu.CreateInstance createInstanceTask, object data = null)
        {
            Jump(menuAsset, menuId, fromMenu, playFocusOutAnimationDefault, destroyInstanceDefault, createInstanceTask, playPushAnimationDefault, data);
        }

        /// <summary>
        /// If fromMenu is not null or empty, pops all above fromMenu and pushes new menu.
        /// If fromMenu is null or empty, pops all and pushes new menu.
        /// If the menuId already exists in the stack, pops all above it.
        /// Fails if fromMenu is not null or empty and doesn't exist in the stack.
        /// </summary>
        /// <param name="menuAsset">The prefab or addressable key to instantiate</param>
        /// <param name="menuId">The id to be associated to this instance</param>
        /// <param name="fromMenu">The id of the menu to jump from</param>
        /// <param name="playFocusOutAnimationTask">The animation to play on the menu that would focus out</param>
        /// <param name="destroyInstanceTask">The function to use to destroy menu instances</param>
        /// <param name="createInstanceTask">The function to use to instantiate this menu</param>
        /// <param name="playPushAnimationTask">The animation to play on the menu that would be pushed</param>
        /// <param name="data">Anything you want to pass to the new menu instance</param>
        public void Jump(object menuAsset, string menuId, string fromMenu, Menu.PlayOutAnimation playFocusOutAnimationTask, Menu.DestroyInstance destroyInstanceTask, Menu.CreateInstance createInstanceTask, Menu.PlayInAnimation playPushAnimationTask, object data = null)
        {
            QueueAction(() => { DoJump(menuAsset, menuId, fromMenu, playFocusOutAnimationTask, destroyInstanceTask, createInstanceTask, playPushAnimationTask, data, ExecuteNextAction).FireAndForgetTask(); });
        }

        /// <summary>
        /// Removes and destroys the topmost menu instance.
        /// </summary>
        public void Pop()
        {
            Pop(playPopAnimationDefault, playFocusInAnimationDefault, destroyInstanceDefault);
        }

        /// <summary>
        /// Removes and destroys the topmost menu instance.
        /// </summary>
        /// <param name="playPopAnimationTask">The animation to play on the menu that would be popped</param>
        /// <param name="playFocusInAnimationTask">The animation to play on the menu that would focus in</param>
        public void Pop(Menu.PlayOutAnimation playPopAnimationTask, Menu.PlayInAnimation playFocusInAnimationTask)
        {
            Pop(playPopAnimationTask, playFocusInAnimationTask, destroyInstanceDefault);
        }

        /// <summary>
        /// Removes and destroys the topmost menu instance.
        /// </summary>
        /// <param name="playPopAnimationTask">The animation to play on the menu that would be popped</param>
        /// <param name="playFocusInAnimationTask">The animation to play on the menu that would focus in</param>
        /// <param name="destroyInstanceTask">The function to use to destroy menu instances</param>
        public void Pop(Menu.PlayOutAnimation playPopAnimationTask, Menu.PlayInAnimation playFocusInAnimationTask, Menu.DestroyInstance destroyInstanceTask)
        {
            QueueAction(() => { DoPop(playPopAnimationTask, playFocusInAnimationTask, destroyInstanceTask, focusOut:true, focusIn:true, ExecuteNextAction).FireAndForgetTask(); });
        }

        /// <summary>
        /// Pops all above menuId.
        /// Fails if menuId doesn't exist in the stack.
        /// </summary>
        /// <param name="menuId">The id of the menu instance to pop to</param>
        public void PopAllAbove(string menuId)
        {
            PopAllAbove(menuId, playPopAnimationDefault, playFocusInAnimationDefault, destroyInstanceDefault);
        }

        /// <summary>
        /// Pops all above menuId.
        /// Fails if menuId doesn't exist in the stack.
        /// </summary>
        /// <param name="menuId">The id of the menu instance to pop to</param>
        /// <param name="playPopAnimationTask">The animation to play on the menu that would be popped</param>
        /// <param name="playFocusInAnimationTask">The animation to play on the menu that would focus in</param>
        public void PopAllAbove(string menuId, Menu.PlayOutAnimation playPopAnimationTask, Menu.PlayInAnimation playFocusInAnimationTask)
        {
            PopAllAbove(menuId, playPopAnimationTask, playFocusInAnimationTask, destroyInstanceDefault);
        }

        /// <summary>
        /// Pops all above menuId.
        /// Fails if menuId doesn't exist in the stack.
        /// </summary>
        /// <param name="menuId">The id of the menu instance to pop to</param>
        /// <param name="playPopAnimationTask">The animation to play on the menu that would be popped</param>
        /// <param name="playFocusInAnimationTask">The animation to play on the menu that would focus in</param>
        /// <param name="destroyInstanceTask">The function to use to destroy menu instances</param>
        public void PopAllAbove(string menuId, Menu.PlayOutAnimation playPopAnimationTask, Menu.PlayInAnimation playFocusInAnimationTask, Menu.DestroyInstance destroyInstanceTask)
        {
            QueueAction(() => { DoPopAllAbove(menuId, playPopAnimationTask, playFocusInAnimationTask, destroyInstanceTask, ExecuteNextAction).FireAndForgetTask(); });
        }

        /// <summary>
        /// Removes and destroys all menu instances.
        /// </summary>
        public void PopAll()
        {
            PopAll(playPopAnimationDefault, destroyInstanceDefault);
        }

        /// <summary>
        /// Removes and destroys all menu instances.
        /// </summary>
        /// <param name="playPopAnimationTask">The animation to play on the menu that would be popped</param>
        public void PopAll(Menu.PlayOutAnimation playPopAnimationTask)
        {
            PopAll(playPopAnimationTask, destroyInstanceDefault);
        }

        /// <summary>
        /// Removes and destroys all menu instances.
        /// </summary>
        /// <param name="playPopAnimationTask">The animation to play on the menu that would be popped</param>
        /// <param name="destroyInstanceTask">The function to use to destroy menu instances</param>
        public void PopAll(Menu.PlayOutAnimation playPopAnimationTask, Menu.DestroyInstance destroyInstanceTask)
        {
            QueueAction(() => { DoPopAll(playPopAnimationTask, destroyInstanceTask, ExecuteNextAction).FireAndForgetTask(); });
        }

        private async Task DoPush(
            object menuAsset,
            string menuId,
            Menu.PlayOutAnimation playOutAnimationTask,
            Menu.CreateInstance createInstanceTask,
            Menu.PlayInAnimation playInAnimationTask,
            object data,
            Action onComplete)
        {
            if (Find(menuId) != null)
            {
                Debug.LogWarning("DoPush: menu with id " + menuId + " already exists in the stack. Call PopAllAbove instead.");

                onComplete?.Invoke();
                return;
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            if(_stack.Count > 0)
            {
                Top.OnFocusOut();
                Top.HasFocus = false;
                OnMenuFocusChanged?.Invoke(Top, false);

                await playOutAnimationTask(Top, _ctSource.Token).ConfigureAwait(true);

                Top.AfterFocusOut.Invoke(Top);
            }

            var instance = await createInstanceTask(menuAsset, menuId, menuAnchor, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            _stack.Add(instance);

            instance.OnPush(this, data);
            OnMenuPushed?.Invoke(instance);

            instance.BeforeFocusIn.Invoke(instance);
            instance.OnFocusIn();
            instance.HasFocus = true;
            OnMenuFocusChanged?.Invoke(instance, true);

            await playInAnimationTask(instance, _ctSource.Token).ConfigureAwait(true);

            if (inputBlocker != null)
                inputBlocker.SetActive(false);

            onComplete?.Invoke();
        }

        private async Task DoJump(
            object menuAsset,
            string menuId,
            string fromMenuId,
            Menu.PlayOutAnimation playOutAnimationTask,
            Menu.DestroyInstance destroyInstanceTask,
            Menu.CreateInstance createInstanceTask,
            Menu.PlayInAnimation playInAnimationTask,
            object data,
            Action onComplete)
        {
            if (Find(menuId) != null)
            {
                await DoPopAllAbove(menuId, playOutAnimationTask, playInAnimationTask, destroyInstanceTask, null).ConfigureAwait(true);

                onComplete?.Invoke();
                return;
            }

            if(string.IsNullOrEmpty(fromMenuId))
            {
                await DoJump(menuAsset, menuId, playOutAnimationTask, destroyInstanceTask, createInstanceTask, playInAnimationTask, data, null).ConfigureAwait(true);

                onComplete?.Invoke();
                return;
            }

            if (Top != null && Top.Id == fromMenuId)
            {
                await DoPush(menuAsset, menuId, playOutAnimationTask, createInstanceTask, playInAnimationTask, data, null).ConfigureAwait(true);

                onComplete?.Invoke();
                return;
            }

            var targetMenuIndex = _stack.FindIndex(m => m.Id == fromMenuId);
            if (targetMenuIndex == -1)
            {
                Debug.LogWarning("DoJump: menu with id " + fromMenuId + " doesnt exist in the stack");

                onComplete?.Invoke();
                return;
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            var instance = await createInstanceTask(menuAsset, menuId, menuAnchor, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            Menu.DeactivateInstance(instance);

            int startIndex = _stack.Count - 1;
            for (int i = startIndex; i > targetMenuIndex; --i)
            {
                await DoPop(playOutAnimationTask, Menu.NoAnimation, destroyInstanceTask, focusOut: i == startIndex, focusIn: false, null).ConfigureAwait(true);
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            _stack.Add(instance);
            Menu.ActivateInstance(instance);

            instance.OnPush(this, data);
            OnMenuPushed?.Invoke(instance);

            instance.BeforeFocusIn.Invoke(instance);
            instance.OnFocusIn();
            instance.HasFocus = true;
            OnMenuFocusChanged?.Invoke(instance, true);

            await playInAnimationTask(instance, _ctSource.Token).ConfigureAwait(true);

            if (inputBlocker != null)
                inputBlocker.SetActive(false);

            onComplete?.Invoke();
        }

        private async Task DoJump(
            object menuAsset,
            string menuId,
            Menu.PlayOutAnimation playOutAnimationTask,
            Menu.DestroyInstance destroyInstanceTask,
            Menu.CreateInstance createInstanceTask,
            Menu.PlayInAnimation playInAnimationTask,
            object data,
            Action onComplete)
        {
            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            var instance = await createInstanceTask(menuAsset, menuId, menuAnchor, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            Menu.DeactivateInstance(instance);

            await DoPopAll(playOutAnimationTask, destroyInstanceTask, null).ConfigureAwait(true);

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            _stack.Add(instance);
            Menu.ActivateInstance(instance);

            instance.OnPush(this, data);
            OnMenuPushed?.Invoke(instance);

            instance.BeforeFocusIn.Invoke(instance);
            instance.OnFocusIn();
            instance.HasFocus = true;
            OnMenuFocusChanged?.Invoke(instance, true);

            await playInAnimationTask(instance, _ctSource.Token).ConfigureAwait(true);

            if (inputBlocker != null)
                inputBlocker.SetActive(false);

            onComplete?.Invoke();
        }

        private async Task DoPop(
            Menu.PlayOutAnimation playOutAnimationTask,
            Menu.PlayInAnimation playInAnimationTask,
            Menu.DestroyInstance destroyInstanceTask,
            bool focusOut,
            bool focusIn,
            Action onComplete)
        {
            if (_stack.Count == 0)
            {
                onComplete?.Invoke();
                return;
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(true);

            if (focusOut)
            {
                Top.OnFocusOut();
                Top.HasFocus = false;
                OnMenuFocusChanged?.Invoke(Top, false);

                await playOutAnimationTask(Top, _ctSource.Token).ConfigureAwait(true);

                Top.AfterFocusOut.Invoke(Top);
            }

            var toPop = Top;
            _stack.RemoveAt(_stack.Count - 1);

            toPop.OnPop();
            OnMenuPopped?.Invoke(toPop);

            await destroyInstanceTask(toPop, this, _ctSource.Token).ConfigureAwait(true);

            if (_ctSource.IsCancellationRequested)
                return;

            if (_stack.Count > 0 && focusIn)
            {
                Top.BeforeFocusIn.Invoke(Top);
                Top.OnFocusIn();
                Top.HasFocus = true;
                OnMenuFocusChanged?.Invoke(Top, true);

                await playInAnimationTask(Top, _ctSource.Token).ConfigureAwait(true);
            }

            if (inputBlocker != null)
                inputBlocker.SetActive(false);

            onComplete?.Invoke();
        }

        private async Task DoPopAllAbove(
            string menuId,
            Menu.PlayOutAnimation playOutAnimationTask,
            Menu.PlayInAnimation playInAnimationTask,
            Menu.DestroyInstance destroyInstanceTask,
            Action onComplete)
        {
            var targetMenuIndex = _stack.FindIndex(m => m.Id == menuId);
            if (targetMenuIndex == -1)
            {
                Debug.LogWarning("DoPopAllAbove: menu with id " + menuId + " doesnt exist in the stack");

                onComplete?.Invoke();
                return;
            }

            int startIndex = _stack.Count - 1;
            for(int i = startIndex; i > targetMenuIndex; --i)
            {
                await DoPop(playOutAnimationTask, playInAnimationTask, destroyInstanceTask, focusOut:i == startIndex, focusIn:i == targetMenuIndex + 1, null).ConfigureAwait(true);
            }

            onComplete?.Invoke();
        }

        private async Task DoPopAll(
            Menu.PlayOutAnimation playOutAnimationTask,
            Menu.DestroyInstance destroyInstanceTask,
            Action onComplete)
        {
            await DoPop(playOutAnimationTask, Menu.NoAnimation, destroyInstanceTask, focusOut:true, focusIn:false, null).ConfigureAwait(true);

            while (_stack.Count > 0)
            {
                await DoPop(playOutAnimationTask, Menu.NoAnimation, destroyInstanceTask, focusOut:false, focusIn:false, null).ConfigureAwait(true);
            }

            onComplete?.Invoke();
        }

        private void QueueAction(Action action)
        {
            _actionQueue.Enqueue(action);

            if (_actionQueue.Count == 1)
            {
                _actionQueue.Peek().Invoke();
            }
        }

        private void ExecuteNextAction()
        {
            _actionQueue.Dequeue();

            if (_actionQueue.Count > 0)
            {
                _actionQueue.Peek().Invoke();
            }
        }
    }
}