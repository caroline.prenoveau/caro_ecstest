﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace HotChocolate.UI
{
    public interface IMenu
    {
        string Id { get; set; }
        bool HasFocus { get; set; }

        Menu.BeforeFocusIn BeforeFocusIn { get; }
        Menu.AfterFocusOut AfterFocusOut { get; }

        void OnPush(MenuStack stack, object data);
        void OnPop();
        void OnFocusIn();
        void OnFocusOut();
    }

    public interface IMenuAnimation
    {
        Task PushAnimation(CancellationToken ct);
        Task PopAnimation(CancellationToken ct);
        Task FocusInAnimation(CancellationToken ct);
        Task FocusOutAnimation(CancellationToken ct);
    }

    public static partial class Menu
    {
        public delegate Task<IMenu> CreateInstance(object asset, string menuId, Transform parent, MonoBehaviour opHolder, CancellationToken ct);
        public delegate Task DestroyInstance(IMenu instance, MonoBehaviour opHolder, CancellationToken ct);

        public delegate Task PlayInAnimation(IMenu instance, CancellationToken ct);
        public delegate Task PlayOutAnimation(IMenu instance, CancellationToken ct);

        public delegate void BeforeFocusIn(IMenu instance);
        public delegate void AfterFocusOut(IMenu instance);

        public static Task<IMenu> CreateInstanceFromPrefab(object prefab, string menuId, Transform parent, MonoBehaviour opHolder, CancellationToken ct)
        {
            if (ct.IsCancellationRequested)
            {
                return null;
            }

            var instance = GameObject.Instantiate(prefab as GameObject, parent);

            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = Vector3.one;

            instance.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            instance.GetComponent<RectTransform>().sizeDelta = Vector2.zero;

            instance.GetComponent<IMenu>().Id = menuId;
            return Task.FromResult(instance.GetComponent<IMenu>());
        }

        public static Task DestroyPrefabInstance(IMenu instance, MonoBehaviour opHolder, CancellationToken ct)
        {
            if (!ct.IsCancellationRequested && instance as MonoBehaviour != null)
            {
                GameObject.Destroy((instance as MonoBehaviour).gameObject);
            }

            return Task.FromResult<bool>(true);
        }

        public static async Task PlayPushAnimationDefault(IMenu instance, CancellationToken ct)
        {
            await PlayPushAnimationDefault(instance as MonoBehaviour, ct);
        }

        public static async Task PlayPushAnimationDefault(MonoBehaviour instance, CancellationToken ct)
        {
            if (instance == null)
                return;

            await PlayPushAnimationDefault(instance.gameObject, ct);
        }

        public static async Task PlayPushAnimationDefault(GameObject instance, CancellationToken ct)
        {
            var animations = instance.GetComponentsInChildren<IMenuAnimation>(false);
            if (animations.Length > 0)
            {
                List<Task> tasks = new List<Task>();
                foreach (var animation in animations)
                {
                    tasks.Add(animation.PushAnimation(ct));
                }

                await Task.WhenAll(tasks).ConfigureAwait(true);
            }
        }

        public static async Task PlayPopAnimationDefault(IMenu instance, CancellationToken ct)
        {
            await PlayPopAnimationDefault(instance as MonoBehaviour, ct);
        }

        public static async Task PlayPopAnimationDefault(MonoBehaviour instance, CancellationToken ct)
        {
            if (instance == null)
                return;

            await PlayPopAnimationDefault(instance.gameObject, ct);
        }

        public static async Task PlayPopAnimationDefault(GameObject instance, CancellationToken ct)
        {
            var animations = instance.GetComponentsInChildren<IMenuAnimation>(false);
            if (animations.Length > 0)
            {
                List<Task> tasks = new List<Task>();
                foreach (var animation in animations)
                {
                    tasks.Add(animation.PopAnimation(ct));
                }

                await Task.WhenAll(tasks).ConfigureAwait(true);
            }
        }

        public static async Task PlayFocusInAnimationDefault(IMenu instance, CancellationToken ct)
        {
            await PlayFocusInAnimationDefault(instance as MonoBehaviour, ct);
        }

        public static async Task PlayFocusInAnimationDefault(MonoBehaviour instance, CancellationToken ct)
        {
            if (instance == null)
                return;

            await PlayFocusInAnimationDefault(instance.gameObject, ct);
        }

        public static async Task PlayFocusInAnimationDefault(GameObject instance, CancellationToken ct)
        {
            var animations = instance.GetComponentsInChildren<IMenuAnimation>(false);
            if (animations.Length > 0)
            {
                List<Task> tasks = new List<Task>();
                foreach (var animation in animations)
                {
                    tasks.Add(animation.FocusInAnimation(ct));
                }

                await Task.WhenAll(tasks).ConfigureAwait(true);
            }
        }

        public static async Task PlayFocusOutAnimationDefault(IMenu instance, CancellationToken ct)
        {
            await PlayFocusOutAnimationDefault(instance as MonoBehaviour, ct);
        }

        public static async Task PlayFocusOutAnimationDefault(MonoBehaviour instance, CancellationToken ct)
        {
            if (instance == null)
                return;

            await PlayFocusOutAnimationDefault(instance.gameObject, ct);
        }

        public static async Task PlayFocusOutAnimationDefault(GameObject instance, CancellationToken ct)
        {
            var animations = instance.GetComponentsInChildren<IMenuAnimation>(false);
            if (animations.Length > 0)
            {
                List<Task> tasks = new List<Task>();
                foreach (var animation in animations)
                {
                    tasks.Add(animation.FocusOutAnimation(ct));
                }

                await Task.WhenAll(tasks).ConfigureAwait(true);
            }
        }

        public static Task NoAnimation(IMenu instance, CancellationToken ct)
        {
            return Task.FromResult<object>(null);
        }

        public static void ActivateInstance(IMenu instance)
        {
            if (instance as MonoBehaviour == null)
                return;

            (instance as MonoBehaviour).gameObject.SetActive(true);
        }

        public static void DeactivateInstance(IMenu instance)
        {
            if (instance as MonoBehaviour == null)
                return;

            (instance as MonoBehaviour).gameObject.SetActive(false);
        }
    }
}
