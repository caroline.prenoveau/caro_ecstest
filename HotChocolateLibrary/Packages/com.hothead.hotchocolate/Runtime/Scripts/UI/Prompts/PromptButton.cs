﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HotChocolate.UI.Prompts
{
    public class PromptButton : MonoBehaviour
    {
        public Button button;
        public TMP_Text label;
    }
}
