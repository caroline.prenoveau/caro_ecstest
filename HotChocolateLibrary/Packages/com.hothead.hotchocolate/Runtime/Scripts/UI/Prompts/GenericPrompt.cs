﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HotChocolate.UI.Prompts
{
    public class GenericPrompt : MonoBehaviour, IMenu
    {
        [Serializable]
        public class PromptButtonByStyle
        {
            public PromptButtonStyle style;
            public PromptButton prefab;
        }

        public List<PromptButtonByStyle> promptButtons = new List<PromptButtonByStyle>();

        public TMP_Text title;
        public TMP_Text subtitle;
        public TMP_Text message;

        public RectTransform buttonsAnchor;
        public List<Button> closeButtons = new List<Button>();

        [HideInInspector]
        public List<PromptButton> buttonInstances = new List<PromptButton>();

        public string Id { get; set; }
        public bool HasFocus { get; set; }

        public Menu.BeforeFocusIn BeforeFocusIn => beforeFocusIn;
        public Menu.BeforeFocusIn beforeFocusIn = Menu.ActivateInstance;

        public Menu.AfterFocusOut AfterFocusOut => afterFocusOut;
        public Menu.AfterFocusOut afterFocusOut = Menu.DeactivateInstance;

        private MenuStack _stack;
        private PromptData _data;

        private void Awake()
        {
            foreach(Transform child in buttonsAnchor)
            {
                Destroy(child.gameObject);
            }
        }

        private void Start()
        {
            foreach (var button in closeButtons)
            {
                button.onClick.AddListener(OnCloseButton);
            }
        }

        public void OnPush(MenuStack stack, object data)
        {
            _stack = stack;
            _data = data as PromptData;

            if (!_data.allowClose)
            {
                foreach (var button in closeButtons)
                {
                    button.gameObject.SetActive(false);
                }
            }

            if (title != null) SetText(title, _data.title);
            if (subtitle != null) SetText(subtitle, _data.subtitle);
            if (message != null) SetText(message, _data.message);

            foreach (var button in _data.buttons)
            {
                var instance = Instantiate(FindButton(button.style), buttonsAnchor);
                buttonInstances.Add(instance);

                instance.label.text = button.label;
                instance.button.onClick.AddListener(() => { OnClickButton(button.onClick); });
            }
        }

        public void OnPop() { }
        public void OnFocusIn() { }
        public void OnFocusOut() { }

        private PromptButton FindButton(PromptButtonStyle style)
        {
            var button = promptButtons.Find(p => p.style == style);

            if (button == null)
                return promptButtons.Find(p => p.style == PromptButtonStyle.Default).prefab;
            else
                return button.prefab;
        }

        private void OnClickButton(Action callback)
        {
            _stack.Pop();
            callback?.Invoke();
        }

        private void OnCloseButton()
        {
            _stack.Pop();
            _data.onCloseButton?.Invoke();
        }

        public void SetText(TMP_Text text, string message)
        {
            if (!string.IsNullOrEmpty(message))
                text.text = message;
            else
                text.gameObject.SetActive(false);
        }
    }
}
