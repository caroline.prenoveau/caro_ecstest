﻿using System;
using System.Collections.Generic;

namespace HotChocolate.UI.Prompts
{
    public enum PromptButtonStyle
    {
        Default,
        Ok,
        Cancel,
        Confirm,
        Yes,
        No
    }

    public class PromptButtonData
    {
        public string label;
        public PromptButtonStyle style;
        public Action onClick;
    }

    public class PromptData
    {
        public string title;
        public string subtitle;
        public string message;
        public bool allowClose;
        public Action onCloseButton;
        public List<PromptButtonData> buttons = new List<PromptButtonData>();
    }
}
