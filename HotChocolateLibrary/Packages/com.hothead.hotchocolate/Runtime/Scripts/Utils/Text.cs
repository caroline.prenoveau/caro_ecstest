﻿using System.Text.RegularExpressions;

namespace HotChocolate.Utils
{
    public static class Text
    {
        public static string InsertSpacesBetweenCaps(string text)
        {
            return Regex.Replace(text, "([a-z])([A-Z])", "$1 $2");
        }
    }
}
