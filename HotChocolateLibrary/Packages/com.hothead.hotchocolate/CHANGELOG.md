## 1.0.3 - 2021-08-04 - (CL-1338662)
### Added
-Exposed ID in IFtueEvent
-Added character name option in tutorial dialogue

### Fixed
-Fixed UI addressable handle getting invalidated when rapidly changing scenes

## 1.0.2 - 2021-07-22 - (CL-1335997)
### Removed
- Removal of cheats. Cheats are now moved to the new HotShot Cheats package. Please view the HotShot Cheats documentation on how to migrate : https://hothead.atlassian.net/wiki/spaces/CT/pages/2126610449/HotShot+Cheats+Package. 

